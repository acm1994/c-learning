// Aaron Christopher Montano
// 05/28/2018
// Root Finding Algorithms: Fixed-Point Iteration.

#include <iostream>
#include <cmath>

// Function that will be passed to the 'fixed' method.
// Param:
// x - x-value (double).
double funct(double x)
{
  return pow(x, 2)/4 + x/2;
}

// Fixed-Point Iteration algorithm.
// Param:
// x0 - initial guess (double).
// n - limit to the number of iterations (integer).
// p - correct to 'p' decimal places (double).
double fixed(double x0, int n, double p)
{

  // Initialize variables.
  int iter = 0;
  double tol = 0.5*pow(10, -p);
  double xi = 0;
  double xPrev = 0;

  // Evaluate g(x) at the initial guess.
  xPrev = x0;
  xi = funct(xPrev);

  /* Begin while loop.  Will terminate when
   * abs(xi - xPrev) < tol. */
  while (std::abs(xi-xPrev) >= tol) {
    
    // Check to see if we have gone past our iteration limit.
    // Otherwise, update. 
    if (iter > n) {
      std::cout << "Fixed-Point Iteration failure: divergence.\n";
      return 0;
    } else {
      xPrev = xi;
      xi = funct(xPrev);
      std::cout << xi << "\n";
      iter++;
    }
  }

  // Print out iteration count and return approximate root.
  std::cout << "Fixed-Point Iteration convergence after " << iter << " iterations.\n";
  std::cout << "r ~ " << xi << ".\n";
  return xi;
}

// Main method.
// Param:
// NONE.
int main()
{
  fixed(1.95, 1000, 6);
  return 0;
}
