// Aaron Christopher Montano
// 05/29/2018
// Root-Finding Algorithms: Newton's Method.

#include <iostream>
#include <cmath>

// Original function.
// Param:
// x - x-value (double).
double funct(double x)
{
  return sqrt(x);
}

// Derivative of original function.
// Param:
// x - x-value (double).
double functP(double x)
{
  return -1/sqrt(x);
}

/* newton. 
 * param:
 * x0 - initial guess (double).
 * n - iteration limit (int).
 * p - correct to 'p' decimal places (double). */
double newton(double x0, int n, double p)
{
  
  // Initialize and define variables.
  int iter = 0;
  double tol = 0.5*pow(10, -p);
  double xi = 0;
  double xPrev = x0;

  // Evaluate initial guess.
  xi = xPrev - funct(xPrev)/functP(xPrev);

  // Loop.
  while (std::abs(xi-xPrev) > tol) {

    // Check to see if we are past our iteration limit.
    // Otherwise, update.
    if (iter > n) {
      std::cout << "Newton Method failure: divergence. \n";
      return 0;
    } else {
      xPrev = xi;
      xi = xPrev - funct(xPrev)/functP(xPrev);
      iter++;
    }
  }

  // Print out iteration count and approximated root.
  std::cout << "Newton Method convergence after " << iter << " iterations. \n";
  std::cout << "r ~ " << xi << ". \n";
  return xi;
} 

// Main method.
int main() {
  newton(0.5, 1000, 8);
  return 0;
}
