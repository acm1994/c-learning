// Aaron Christopher Montano
// 05/26/2018
// Root Finding: Bisection Method.

#include <iostream>
#include <cmath>

// Function of interest.
// Parameters:
// x - x-value.
double f(double x)
{
  return 4-pow(x, 2);
}

// Bisection algorithm.
// Parameters:
// a - left-hand bound.
// b - right-hand bound.
// p - tolerance (i.e., correct up to 'p' decimal points.
double bisect(double a, double b, double p)
{

  // Initialize important variables.
  double tol = 0.5*pow(10, -p);
  int iter = 0;
  double c, del;

  /* Check to see if our interval contains a root on it. If so,
     continue on into the main algorithm.  Otherwise, exit the method
     and return an error message. */
  if (f(a)*f(b) > 0) {
    
    std::cout << "Bisection Method failure: interval does not contain a root. \n";
    return 0;
    
  } else {
    
    // Evaluate midpoint of interval and repeat iterations.
    c = (a+b)/2;
    del = (b-a)/2;

    // Main loop.
    while (del >= tol) {
      
      // Initial Test.
      if (f(a) < f(c)) {
	b = c;
      } else {
	a = c;
      }

      // Update intervals.
      c = (a+b)/2;
      del = (b-a)/2;
      std::cout << c << "\n";
      iter++;
    }

    // Return final output.
    std::cout << "Bisection Method conergence in " << iter << " iterations.\n";
    std::cout << "r ~ " << c << "\n";
    return c;
  }

}

// Main method.
int main()
{

  // Initialize variables to be used in the method.
  double a, b, p;
  a = 1;
  b = 7;
  p = 7;

  // Perform method.
  bisect(a, b, p);
  
  return 0;
}

