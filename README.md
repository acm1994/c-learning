# C++ Learning

## This repository contains all of the code created while learning the basics of the C++ programming language.

## The book Sams Teach Yourself C++ in 24 Hours is used as a guide for my learning.

## Any code in this repository that is similar or identical to the code in the book mentioned earlier is expected; I do not claim ownership of any code from the textbook.

## The majority of the code in this repository will be the C++ version of the programs created in MATH / CS 375 at the University of New Mexico.  The original code for the course can be found in the respective repository.
